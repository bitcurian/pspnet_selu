#!/usr/bin/env sh
set -e

TOOLS=./build/tools

$TOOLS/caffe train \
    --solver=evaluation/prototxt/solver_pspnet101_cityscapes_713.prototxt --gpu=0,1 $@

